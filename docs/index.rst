Welcome to Jugas IT's MyCollection documentation!
=================================================

.. toctree::
   :maxdepth: 2
   :caption: User documentation

   README
   playbooks/index
   roles/index

.. toctree::
   :maxdepth: 2
   :caption: Developer documentation

   contributing
   testing
   releasing

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
