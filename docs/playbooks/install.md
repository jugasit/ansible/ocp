# jugasit.ocp.install

This playbook installs OpenShift.

## Requirements

See the [Common Role Requirements](https://gitlab.com/jugasit/ansible/ocp/-/blob/main/README.md#common-role-requirements).

## Usage

```yaml
- import_playbook: jugasit.ocp.install
```

### Available tags

Control execution by including or skipping the following tags:

- `foo` - Do foo.

## Configuration

Place the variables for configuring the playbook either using the `var` keyword directly under `import_playbook`, or
put the variables in either groups or hosts inside the inventory.

Example:

```yaml
- import_playbook: jugasit.ocp.install
  vars:
    foo: 123
```
