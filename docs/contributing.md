# Contributing

Contributions to this collection are very appreciated.

The source code is available on [GitLab](https://gitlab.com/jugasit/ansible/ocp).
Any bugs or suggestions are reported as [Issues](https://gitlab.com/jugasit/ansible/ocp/-/issues).

## Before developing

Create a Python virtual environment:

```shell
python3 -m venv ./venv
```

Activate it before developing:

```shell
source ./venv/bin/activate
```

Install all Python packages required for developing:

```shell
pip3 install -r requirements.txt
```

## Setting environment variables

You need to set the following environment variables prior to executing `molecule`:

```shell
export GCP_PROJECT=my-project-1234567
export GCP_CREDENTIALS=$PWD/tmp/gcp_credentials.json
```
