jugasit.ocp.my_resource
=======================

This role creates, manages and deletes my_resource.

Requirements
------------

See the [Common Role Requirements](https://gitlab.com/jugasit/ansible/ocp/-/blob/main/README.md#common-role-requirements).

Role Variables
--------------

This role supports the [Common Role Variables](https://gitlab.com/jugasit/ansible/ocp/-/blob/main/README.md#common-role-variables).

The main data structure for this role is the list of `ocp_my_resources`. Each `my_resource` requires the following fields:

- `name`: The name of the my_resource.

In addition the following optional fields are available:

- `foo`: A short description.

Example Playbook
----------------

Create a basic my_resource:

```yaml
- name: Create my_resource
  hosts: localhost
  connection: local
  gather_facts: false
  vars:
    my_resources:
      - name: my_resource-1
        foo: Short description.
  roles:
    - jugasit.ocp.my_resource
```
