# Red Hat OpenShift Platform Ansible Roles

Ansible playbooks and roles for doing installing and configuring Red Hat OpenShift Platform.

## Installation

You can install the collection from [Ansible Galaxy](https://galaxy.ansible.com/jugasit/ocp) by running `ansible-galaxy collection install jugasit.ocp`.

## Usage

### Roles

After the installation, the roles are available as `jugasit.ocp.<role_name>`.

For example:

```yaml
- name: Do some foo
  hosts: localhost
  connection: local
  roles:
    - jugasit.ocp.my_resource
```

Please see the [Using Ansible collections documentation](https://docs.ansible.com/ansible/devel/collections_guide/index.html#collections-index) for further details.

### Playbooks

To use the playbooks:

```yaml
- import_playbook: jugasit.ocp.install
```

### Reference

For the full reference of all roles and playbooks available see [our documentation](https://jugasit.gitlab.io/ansible/ocp).

### Common Role Requirements

#### Packages

The roles in this collection uses modules from the `xxx` collection which requires the following packages to be installed:

```shell
sudo dnf install foo
```

### Common Role Variables

- `foo`: Foo.
- `ocp_state`: The state of the resources. Default is `present`.
